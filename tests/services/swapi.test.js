import { swapiService, cache } from '../../src/services'
import MockAdapter from 'axios-mock-adapter'

const releaseDates = {
  first: '2020-01-01',
  second: '2020-01-02',
  third: '2020-01-03',
}

const mockCharacterUrls = [
  'http://swapi.dev/api/people/3/',
  'http://swapi.dev/api/people/4/',
]

const mockFilms = [
  {
    episode_id: 1,
    title: 'foo bar 1',
    release_date: releaseDates.second,
    characters: mockCharacterUrls,
    foo: 'bar',
  },
  {
    episode_id: 2,
    title: 'foo bar 2',
    release_date: releaseDates.third,
    foo: 'bar',
  },
  {
    episode_id: 3,
    title: 'foo bar 3',
    release_date: releaseDates.first,
    foo: 'bar',
  },
]

const mockFilmResponseData = {
  results: mockFilms,
}

const mockCharacterResponseData = {
  height: 'tall',
  name: 'foo bar 1',
  foo: 'bar',
}

const fakeCachedFilms = [
  {
    foo: 'bar',
  },
  {
    bar: 'foo',
  },
]

const FILMS_CACHE_KEY = 'films'

beforeAll(async () => {
  await cache.redisClient.flushall()
})
afterAll(() => {
  cache.redisClient.disconnect()
})
afterEach(async () => {
  await cache.redisClient.flushall()
})
describe('swapiService', () => {
  let axiosMock
  beforeEach(() => {
    axiosMock = new MockAdapter(swapiService.apiClient)
    axiosMock.onGet('films/').reply(200, mockFilmResponseData)

    const filmsUrl = /films\/*/
    axiosMock.onGet(filmsUrl).reply(200, mockFilms[0])

    const charactersUrl = /people\/*/
    axiosMock.onGet(charactersUrl).reply(200, mockCharacterResponseData)
  })

  describe('getAllFilms', () => {
    it('returns array of films', async () => {
      const result = await swapiService.getAllFilms()

      expect(result).toHaveLength(3)

      for (const film of result) {
        expect(film.id).toEqual(expect.any(Number))
        expect(film.release_date).toEqual(expect.any(String))
        expect(film.title).toEqual(expect.any(String))
      }
    })
    it('caches the films', async () => {
      const cacheSpy = jest.spyOn(cache, 'storeObject')
      const films = await swapiService.getAllFilms()
      expect(cacheSpy).toHaveBeenCalledTimes(4) // one key per episode, one key for all films

      const cachedFilms = await cache.retrieveObject(FILMS_CACHE_KEY)
      expect(cachedFilms).toBeTruthy()

      for (const film of films) {
        const cachedFilm = await cache.retrieveObject(`films.${film.id}`)
        expect(cachedFilm).toBeTruthy()
      }
    })
    it('uses cached films if present', async () => {
      await cache.storeObject(FILMS_CACHE_KEY, fakeCachedFilms, 10)

      const result = await swapiService.getAllFilms()
      expect(result).toEqual(fakeCachedFilms)
    })
  })

  describe('getFilmCharacters', () => {
    it('returns array of characters', async () => {
      const result = await swapiService.getFilmCharacters(3)
      expect(result).toHaveLength(2) // length of mockCharacterUrls
    })

    it('caches the characters', async () => {
      const cacheSpy = jest.spyOn(cache, 'storeObject')
      await swapiService.getFilmCharacters(3)
      expect(cacheSpy).toHaveBeenCalledTimes(3) // cached both characters + film
      const cachedFilmKey = 'films.3'
      const cachedFilm = await cache.retrieveObject(cachedFilmKey)
      expect(cachedFilm).toBeTruthy()
    })
    it('uses cached characters if present', async () => {
      const firstCharacterCacheKey = 'characters.3'
      const secondCharacterCacheKey = 'characters.4'
      const mockCharacter = { name: 'foobar', favoriteColor: 'green' }
      await cache.storeObject(firstCharacterCacheKey, mockCharacter, 10)
      await cache.storeObject(secondCharacterCacheKey, mockCharacter, 10)

      const characters = await swapiService.getFilmCharacters(3)
      expect(characters).toHaveLength(2)
      for (const character of characters) {
        expect(character.name).toBe('foobar')
        expect(character.favoriteColor).toBe('green')
      }
    })
  })

  describe('getFilm', () => {
    it('uses the filmIds associated episode_id to call the api', async () => {
      await swapiService.getFilm(3)
      expect(axiosMock.history.get).toHaveLength(1)
      expect(axiosMock.history.get[0].url).toBe('films/1') // the 4th star wars movie is episode 1
    })

    it('caches the film', async () => {
      const cacheSpy = jest.spyOn(cache, 'storeObject')
      await swapiService.getFilm(3)
      expect(cacheSpy).toHaveBeenCalledTimes(1)
      const cachedFilmKey = 'films.3'
      const cachedFilm = await cache.retrieveObject(cachedFilmKey)
      expect(cachedFilm).toBeTruthy()
    })

    it('uses cached film if present', async () => {
      const cachedFilmKey = 'films.3'
      await cache.storeObject(cachedFilmKey, fakeCachedFilms[0], 10)
      const film = await swapiService.getFilm(3)
      expect(film).toEqual(fakeCachedFilms[0])
    })
  })
})
