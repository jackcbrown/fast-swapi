import { cache } from '../../src/services'

describe('cache', () => {
  it('retrieves the same object it stored with given ttl', async () => {
    const obj = { foo: 'bar' }
    const key = 'key'
    const ttl = 10
    await cache.storeObject(key, obj, ttl)

    const actualTtl = await cache.redisClient.ttl(key)

    // means a ttl is set
    // see https://redis.io/commands/ttl
    expect(actualTtl).toBeGreaterThan(0)

    const value = await cache.retrieveObject(key)
    expect(value).toEqual(obj)
  })

  it('retrieves the same value it stored with given ttl', async () => {
    const obj = 'foobar'
    const key = 'key'
    const ttl = 10
    await cache.storeValue(key, obj, ttl)

    const actualTtl = await cache.redisClient.ttl(key)

    // means a ttl is set
    // see https://redis.io/commands/ttl
    expect(actualTtl).toBeGreaterThan(0)

    const value = await cache.retrieveValue(key)
    expect(value).toEqual(obj)
  })

  it('defaults to no ttl', async () => {
    const val = 'foobar'
    const key = 'key2'
    await cache.storeObject(key, val)

    const actualTtl = await cache.redisClient.ttl(key)

    // means a ttl is not set
    // see https://redis.io/commands/ttl
    expect(actualTtl).toBe(-1)
  })
})
afterAll(async () => {
  await cache.redisClient.flushall()
  cache.redisClient.disconnect()
})
