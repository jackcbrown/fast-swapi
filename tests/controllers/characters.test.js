import { characterController } from '../../src/controllers'
import { cache, swapiService } from '../../src/services'

const mockRes = {
  json: jest.fn(),
}

const mockSwapiData = [
  { id: 1, name: 'foo bar 1', height: 'tall', foo: 'bar' },
  { id: 2, name: 'foo bar 2', height: 'short', bar: 'foo' },
]

const fakeRequest = {
  body: {
    filmID: 2,
  },
}

afterAll(async () => {
  await cache.redisClient.flushall()
  cache.redisClient.disconnect()
})
describe('characterController', () => {
  beforeEach(() => {
    jest
      .spyOn(swapiService, 'getFilmCharacters')
      .mockResolvedValue(mockSwapiData)
  })
  describe('getAllFilms', () => {
    it('returns specific fields from swapiService', async () => {
      await characterController.getCharactersByFilm(fakeRequest, mockRes)
      const returnedFilmData = mockRes.json.mock.calls[0][0]

      expect(returnedFilmData).toHaveLength(2)

      for (const filmData of returnedFilmData) {
        expect(filmData).toMatchObject({
          id: expect.any(Number),
          name: expect.any(String),
        })
      }
    })
  })
})
