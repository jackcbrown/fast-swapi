import { filmController } from '../../src/controllers'
import { cache, swapiService } from '../../src/services'

const mockRes = {
  json: jest.fn(),
}

const mockSwapiData = [
  { id: 1, title: 'foo bar 1', release_date: 'soon', foo: 'bar' },
  { id: 2, title: 'foo bar 2', release_date: 'later', bar: 'foo' },
]

afterAll(async () => {
  await cache.redisClient.flushall()
  cache.redisClient.disconnect()
})
describe('filmController', () => {
  beforeEach(() => {
    jest.spyOn(swapiService, 'getAllFilms').mockResolvedValue(mockSwapiData)
  })
  describe('getAllFilms', () => {
    it('returns specific fields from swapiService', async () => {
      await filmController.getAllFilms(null, mockRes)
      const returnedFilmData = mockRes.json.mock.calls[0][0]

      expect(returnedFilmData).toHaveLength(2)

      for (const filmData of returnedFilmData) {
        expect(filmData).toMatchObject({
          id: expect.any(Number),
          title: expect.any(String),
          release_date: expect.any(String),
        })
      }
      // expect(mockRes.json).toHaveBeenCalledWith()
    })
  })
})
