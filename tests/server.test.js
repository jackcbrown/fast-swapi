import MockAdapter from 'axios-mock-adapter'
import request from 'supertest'
import server from '../src/server'
import { cache, swapiService } from '../src/services'

const releaseDates = {
  first: '2020-01-01',
  second: '2020-01-02',
  third: '2020-01-03',
}

const mockCharacterUrls = [
  'http://swapi.dev/api/people/3/',
  'http://swapi.dev/api/people/4/',
]

const mockFilmResponseData = {
  results: [
    {
      episode_id: 1,
      title: 'foo bar 1',
      release_date: releaseDates.second,
      characters: mockCharacterUrls,
      foo: 'bar',
    },
    {
      episode_id: 2,
      title: 'foo bar 2',
      release_date: releaseDates.third,
      characters: mockCharacterUrls,
      foo: 'bar',
    },
    {
      episode_id: 3,
      title: 'foo bar 3',
      release_date: releaseDates.first,
      characters: mockCharacterUrls,
      foo: 'bar',
    },
  ],
}

const mockCharacterResponseData = {
  name: 'yoda',
  height: 'short',
  foo: 'bar',
}

afterEach(async () => {
  await cache.redisClient.flushall()
})
afterAll(async () => {
  server.close()
  cache.redisClient.disconnect()
})
describe('GET /films', () => {
  beforeEach(() => {
    const axiosMock = new MockAdapter(swapiService.apiClient)
    axiosMock.onGet('films/').reply(200, mockFilmResponseData)
  })
  it('matches expected structure', async () => {
    const response = await request(server).get('/films')
    expect(response.status).toBe(200)
    expect(response.body).toHaveLength(3)
    for (const film of response.body) {
      expect(film).toEqual({
        id: expect.any(Number),
        release_date: expect.any(String),
        title: expect.any(String),
      })
    }
  })
})

describe('POST /characters', () => {
  beforeEach(() => {
    const axiosMock = new MockAdapter(swapiService.apiClient)

    const filmsUrl = /films\/*/
    axiosMock.onGet(filmsUrl).reply(200, mockFilmResponseData.results[0])

    const charactersUrl = /people\/*/
    axiosMock.onGet(charactersUrl).reply(200, mockCharacterResponseData)
  })
  it('matches expected structure', async () => {
    const response = await request(server)
      .post('/characters')
      .send({ filmID: 3 })
    expect(response.status).toBe(200)
    expect(response.body).toHaveLength(2)
    for (const character of response.body) {
      expect(character).toEqual({
        id: expect.any(Number),
        name: expect.any(String),
      })
    }
  })
})
