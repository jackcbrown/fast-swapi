# Fast SWAPI

Interface for the Star Wars API. Uses Redis to cache responses from the API.

## Getting started

Requirements:
* docker
* docker-compose

To start the application, run the following
```sh
docker-compose up -d
```

Once that's up, try making a call to GET http://localhost:8080/films

## Endpoints

| Url | Method | Description |
|-|-|-|
| films | GET | Get all films. Use these IDs for the characters endpoint. |
| characters | POST | Get characters in a specific film. JSON body:  `{filmID: <Number>}`. *Note: film ID is not episode ID* |

## Code samples

### Get all films
```js
var axios = require("axios").default;

var options = {method: 'GET', url: 'http://localhost:8080/films'};

axios.request(options).then(function (response) {
  console.log(response.data);
}).catch(function (error) {
  console.error(error);
});
```

### Get film characters
```js
var axios = require("axios").default;

var options = {
  method: 'POST',
  url: 'http://localhost:8080/characters/',
  headers: {'content-type': 'application/json'},
  data: {filmID: 3}
};

axios.request(options).then(function (response) {
  console.log(response.data);
}).catch(function (error) {
  console.error(error);
});
```

## Development

Included are npm scripts for your convenience:
* `npm test`
  - runs your code against the unit tests. You'll want to make sure you have redis up and running (`docker-compose up -d redis`)
* `npm run lint:fix`
  - fix linting errors
* `npm run build`
  - transpiles your code to use with node
* `npm start`
  - runs your transpiled code

You can also start your app with `docker-compose up -d` and view the logs with `docker-compose logs`.

## TODO

- [ ] refactor tests to share mock data
- [ ] write Film class
- [ ] write Character class
