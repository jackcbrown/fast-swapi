const { cache } = require('./src/services')

async function clearCache() {
  await cache.redisClient.flushall()
}

module.exports = clearCache
