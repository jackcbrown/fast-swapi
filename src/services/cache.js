import Redis from 'ioredis'

const REDIS_HOST = process.env.REDIS_HOST || 'localhost'
const REDIS_PORT = process.env.REDIS_PORT || 6379

class Cache {
  constructor() {
    this.redisClient = new Redis(REDIS_PORT, REDIS_HOST)
  }

  async retrieveValue(key) {
    const value = await this.redisClient.get(key)
    return value
  }

  async retrieveObject(key) {
    const value = await this.retrieveValue(key)
    return JSON.parse(value)
  }

  /**
   *
   * @param {String} key
   * @param {Object} value
   * @param {Number=} ttl - seconds until key expires
   */
  storeObject(key, value, ttl) {
    const storedValue = JSON.stringify(value)
    return this.storeValue(key, storedValue, ttl)
  }

  /**
   *
   * @param {String} key
   * @param {String} value
   * @param {Number=} ttl - seconds until key expires
   */
  storeValue(key, value, ttl) {
    if (ttl) {
      return this.redisClient.set(key, value, 'EX', ttl)
    }
    return this.redisClient.set(key, value)
  }
}

const cache = new Cache()

export default cache
