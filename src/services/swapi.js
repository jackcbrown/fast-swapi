import axios from 'axios'
import { cache } from './'

const BASE_URL = 'https://swapi.dev/api/'

const FILMS_CACHE_KEY = 'films'
const CHARACTERS_CACHE_KEY = 'characters'

/**
 *
 * @param {Number} filmId
 */
const getFilmCacheKey = (filmId) => `${FILMS_CACHE_KEY}.${filmId}`

/**
 *
 * @param {Number} characterId
 */
const getCharacterCacheKey = (characterId) =>
  `${CHARACTERS_CACHE_KEY}.${characterId}`

const FILMS_CACHE_TTL = 3600 // 1 hour
const CHARACTER_CACHE_TTL = 3600 // 1 hour

// used to map `episode_id` to `id` returned by our service
const epsiodeIdFilmIdMapping = {
  1: 3,
  2: 4,
  3: 5,
  4: 0,
  5: 1,
  6: 2,
}

class SwapiClient {
  constructor() {
    this.apiClient = axios.create({ baseURL: BASE_URL })
  }

  /**
   *
   * @param {Number} episodeId
   * @returns {Number} filmId
   */
  getFilmIdFromEpisodeId(episodeId) {
    return epsiodeIdFilmIdMapping[episodeId]
  }

  /**
   *
   * @param {Number} filmId
   * @returns {Number} episodeId
   */
  getEpisodeIdFromFilmId(filmId) {
    return Object.keys(epsiodeIdFilmIdMapping).find(
      (episodeId) => epsiodeIdFilmIdMapping[episodeId] === filmId
    )
  }

  /**
   * Get all star wars films
   */
  async getAllFilms() {
    // check to see if films are cached before making API request
    const cachedFilms = await cache.retrieveObject(FILMS_CACHE_KEY)

    if (cachedFilms) {
      return cachedFilms
    }

    // if not cached, call the API then cache the result
    const response = await this.apiClient.get('films/')
    const results = response.data?.results || []

    const films = results.map((film) => ({
      id: this.getFilmIdFromEpisodeId(film.episode_id),
      ...film,
    }))

    // cache each film individually (for faster calls to getFilmCharacters)
    const filmCachePromises = films.map((film) => {
      const filmCacheKey = getFilmCacheKey(film.id)
      return cache.storeObject(filmCacheKey, film, FILMS_CACHE_TTL)
    })

    // cache all films for faster calls to getAllFilms
    const allFilmsCachePromise = cache.storeObject(
      FILMS_CACHE_KEY,
      films,
      FILMS_CACHE_TTL
    )

    filmCachePromises.push(allFilmsCachePromise)

    await Promise.all(filmCachePromises)

    return films
  }

  /**
   * Get specific star wars film
   * @param {Number} filmId
   */
  async getFilm(filmId) {
    // check to see if film is cached before making API request
    const filmCacheKey = getFilmCacheKey(filmId)
    const cachedFilm = await cache.retrieveObject(filmCacheKey)

    if (cachedFilm) {
      return cachedFilm
    }

    // if not cached, call the API then cache the result
    const episodeId = this.getEpisodeIdFromFilmId(filmId)
    const response = await this.apiClient.get(`films/${episodeId}`)
    const result = response.data

    const film = { id: filmId, ...result }

    await cache.storeObject(filmCacheKey, film, FILMS_CACHE_TTL)

    return film
  }

  /**
   * Get specific star wars character
   * @param {Number} characterId
   */
  async getCharacter(characterId) {
    // check to see if character is cached before making API request
    const characterCacheKey = getCharacterCacheKey(characterId)
    const cachedCharacter = await cache.retrieveObject(characterCacheKey)

    if (cachedCharacter) {
      return cachedCharacter
    }

    // if not cached, call the API then cache the result
    const response = await this.apiClient.get(`people/${characterId}`)
    const character = response.data

    await cache.storeObject(characterCacheKey, character, CHARACTER_CACHE_TTL)
    return character
  }

  /**
   * Get star wars characters in a specific film
   * @param {Number} filmId
   */
  async getFilmCharacters(filmId) {
    const film = await this.getFilm(filmId)

    const characterUrls = film.characters
    const characterPromises = characterUrls.map((characterUrl) => {
      // parse characterId from url (e.g. http://swapi.dev/api/people/3/)
      const urlParts = characterUrl.split('/')
      const characterId = parseInt(urlParts[urlParts.length - 2])
      return this.getCharacter(characterId)
    })

    // get the characters in parallel
    const results = await Promise.all(characterPromises)
    const characters = results.map((character, i) => ({
      id: i, // generate our own id
      ...character,
    }))
    return characters
  }
}

const swapiClient = new SwapiClient()

export default swapiClient
