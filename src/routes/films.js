import express from 'express'
import { filmController } from '../controllers'

const filmRouter = express.Router()

// get all films
filmRouter.get('/', filmController.getAllFilms)

export default filmRouter
