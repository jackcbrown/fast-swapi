import filmRouter from './films'
import characterRouter from './characters'

/**
 * @typedef {import('express').Express} Server
 * @param {Server} server
 */
function useRoutes(server) {
  server.use('/films', filmRouter)
  server.use('/characters', characterRouter)
}

export default useRoutes
