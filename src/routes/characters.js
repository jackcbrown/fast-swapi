import express from 'express'
import { characterController } from '../controllers'

const characterRouter = express.Router()

// get characters in a film
characterRouter.post('/', characterController.getCharactersByFilm)

export default characterRouter
