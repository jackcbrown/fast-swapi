import { swapiService } from '../services'

async function getCharactersByFilm(req, res) {
  const { filmID: filmId } = req.body
  const characters = await swapiService.getFilmCharacters(filmId)

  const returnedCharacters = characters.map((character) => ({
    id: character.id,
    name: character.name,
  }))

  res.json(returnedCharacters)
}

export { getCharactersByFilm }
