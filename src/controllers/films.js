import { swapiService } from '../services'

async function getAllFilms(_, res) {
  const films = await swapiService.getAllFilms()

  const returnedFilms = films.map((film) => ({
    id: film.id,
    title: film.title,
    release_date: film.release_date,
  }))

  res.json(returnedFilms)
}

export { getAllFilms }
