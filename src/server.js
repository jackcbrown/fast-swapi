import express from 'express'
import useRoutes from './routes'

const app = express()
const PORT = process.env.PORT || 8080

app.use(express.json()) // for parsing application/json

useRoutes(app)

const server = app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}.`)
})

export default server
