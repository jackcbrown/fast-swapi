FROM node:14-alpine

# copy only our dependencies first so docker will cache
# the next step in the build
COPY ["package.json", "package-lock.json", "./"]
# note: this step will be cached until we update the dependencies (even dev depencies)
RUN npm ci

ENV NODE_ENV=production

COPY . .

RUN npm run build

## now remove dev-dependencies
RUN npm prune --production

CMD ["npm", "start"]
